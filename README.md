# Naive Bayes on Amazon Fine Food Reviews

Using the Naive Bayes Model to predict the polarity of a review based on its review text.